package Model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MonitoredData {
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    public Long getDuration(){
        return Duration.between(this.startTime,this.endTime).getSeconds();
    }

    @Override
    public String toString() {
        return  "startTime=" + startTime.format(formatter) +
                ", endTime=" + endTime.format(formatter) +
                ", activity='" + activity + '\'';
    }
}
