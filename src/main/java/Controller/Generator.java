package Controller;

import Model.MonitoredData;

import java.io.*;
import java.util.List;
import java.util.Map;

public class Generator {
    private OutputStream os;

    public int generate(List<MonitoredData> data){
        try {
            os = new FileOutputStream("Task_1.txt");
            final PrintStream printStream = new PrintStream(os);
            data.forEach(printStream::println);
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }
    public int generate(Long distinctDays){
        try {
            os = new FileOutputStream("Task_2.txt");
            final PrintStream printStream = new PrintStream(os);
            printStream.println("Distinct days: "+distinctDays);
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public int generate_t3(Map<String,Long> map){
        try {
            os = new FileOutputStream("Task_3.txt");
            final PrintStream printStream = new PrintStream(os);
            map.forEach((a,n)->printStream.println("Activity: "+a+" appears "+n+" times."));
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public int generate_t4(Map<Integer,Map<String,Long>> map){
        try {
            os = new FileOutputStream("Task_4.txt");
            final PrintStream printStream = new PrintStream(os);
            map.forEach((d,m)-> printStream.println("Day: "+d+" Activity: "+m));
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public int generate_t5(Map<String, Long> map){
        try {
            os = new FileOutputStream("Task_5.txt");
            final PrintStream printStream = new PrintStream(os);
            map.forEach((d,m)-> printStream.println("Activity: "+d+" Duration: "+longToDuration(m)));
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public int generate_t6(List<String> map){
        try {
            os = new FileOutputStream("Task_6.txt");
            final PrintStream printStream = new PrintStream(os);
            map.forEach(e -> printStream.println("Activity with 90% time under five minutes: "+e));
            printStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    public String longToDuration(Long seconds){
        long hours = seconds / 3600;
        long minutes = ((seconds % 3600) / 60);
        long secs = (seconds % 60);
        long[] time = {hours, minutes, secs};
        return time[0] + " hours "
                + time[1] + " minutes "
                + time[2] + " seconds.";
    }
}
