package Controller;

import Model.MonitoredData;

import java.io.*;
import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Tasks {
    public ArrayList<MonitoredData> Task_1(String inputFile){
        ArrayList<MonitoredData> data = new ArrayList<>();
        try {
            String line;
            DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(inputFile)));
            while((line = in.readLine())!= null) {
                String[] components = line.split("\t\t");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime st = LocalDateTime.parse(components[0], formatter);
                LocalDateTime fn = LocalDateTime.parse(components[1], formatter);
                MonitoredData row = new MonitoredData(st, fn, components[2]);
                data.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public long Task_2(List<MonitoredData> data){
        List<Integer> distinctStartDays = data.stream()
                .filter(distinctByKey(e->e.getStartTime().getDayOfYear()))
                .map(s->s.getStartTime().getDayOfYear())
                .collect(Collectors.toList());
        List<Integer> distinctEndDays = data.stream()
                .filter(distinctByKey(e->e.getEndTime().getDayOfYear()))
                .map(s->s.getEndTime().getDayOfYear())
                .collect(Collectors.toList());
        distinctStartDays.forEach(e->{
            if(!distinctEndDays.contains(e))
                distinctEndDays.add(e);
        });

        return distinctEndDays.stream().count();
    }

    static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public Map<String, Long> Task_3(List<MonitoredData> data){
        Map<String, Long> myMap = data.stream()
                .collect(groupingBy(a -> a.getActivity(), Collectors.counting()));
        return myMap;
    }

    public Map<Integer,Map<String,Long>> Task_4(List<MonitoredData> data){
        Map<Integer,Map<String,Long>> myMap=data.stream()
                .collect(groupingBy(d->d.getStartTime().getDayOfYear(), groupingBy(a -> a.getActivity(), Collectors.counting())));
        return myMap;
    }

    public Map<String, Long> Task_5(List<MonitoredData> data){
        Map<String,Long> myMap=data.stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingLong(MonitoredData::getDuration)));
        return myMap;
    }

    public List<String> Task_6(List<MonitoredData> data){
        Map<String,Long> allData=data.stream()
                .collect(groupingBy(d->d.getActivity(),Collectors.counting()));
        Map<String,Long> belowFive=data.stream()
                .filter(d->d.getDuration() < 300)
                .collect(groupingBy(d->d.getActivity(),Collectors.counting()));
        List<String> myList = new ArrayList<>();
        belowFive.forEach((d,m)-> {
            if(m/allData.remove(d) > 0.9)
                myList.add(d);
        });
        return myList;
    }

}
