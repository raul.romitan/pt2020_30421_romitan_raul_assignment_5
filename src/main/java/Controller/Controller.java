package Controller;

import Model.MonitoredData;

import java.util.List;

public class Controller {
    private Tasks tasks = new Tasks();
    private List<MonitoredData> data;
    private Generator g = new Generator();

    public Controller(String inputFile) {
        //TASK 1
        this.data=tasks.Task_1(inputFile);
        g.generate(data);
        /*
        long distinctDays = tasks.Task_2(data);
        System.out.println("Distinct days: " + distinctDays);
         */

        //TASK 2
        g.generate(tasks.Task_2(data));
        /*
        Map<String,Long> t3_map = tasks.Task_3(data);
        t3_map.forEach((a,n)->System.out.println("Activity: "+a+" appears "+n+" times."));
         */
        //TASK 3
        g.generate_t3(tasks.Task_3(data));

        /*
        Map<Integer,Map<String,Long>> t4_map=tasks.Task_4(data);
        t4_map.forEach((d,m)-> System.out.println("Day: "+d+" Activity: "+m));
         */
        //TASK 4
        g.generate_t4(tasks.Task_4(data));

        /*
        Map<String, Long> t5_map=tasks.Task_5(data);
        t5_map.forEach((d,m)-> System.out.println("Activity: "+d+" Duration: "+longToDuration(m)));
         */
        //TASK 5
        g.generate_t5(tasks.Task_5(data));

        /*
        List<String> t6_map=tasks.Task_6(data);
        t6_map.forEach(e->System.out.println("Activity with average time under five minutes: "+e));
        */
        //TASK 6
        g.generate_t6(tasks.Task_6(data));
    }
}
